
# coding: utf-8

# In[5]:

##### AP156 activity 7
##### Ferrer, Nica Jane B.
#### 2013-25765
#!/usr/bin/env python

"""
Code for Chaotic Pendulum (W.Kinzel/G.Reents, Physics by Computer)

This code is based on pendulum.c listed in Appendix E of the book and will
replicate Fig. 4.3 of the book.
"""

__author__ = "Christian Alis"
__credits__ = "W.Kinzel/G.Reents"

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from numpy import sin, cos
from scipy.integrate import odeint

class Simulation(object):
    r = 0.25
    a = 0.7
    lims = 3.0
    dt = 0.1
    pstep = 3*np.pi
    poncaire = False

    def derivs(self, t, y):
        # add a docstring to this function
        """ 
        Calculates the components of f from t and y where:
        f[0] = y[1]
        f[1] = -r*y[1] - sin(y[0]) + a *cos (omega_D * t)
        Parameters
        ----------
        t : float
            value of the constant t
        y: array
            An array of y values
        Returns
        -------
        An array of length 2 which contains the value of the two components of f.
        """
        return np.array([y[1], - self.r*y[1] - sin(y[0]) + self.a*cos(2./3.*t)])
    
    def rk4_step(self, f, x, y, h):
        # add a docstring to this function
        """ Takes one Runge-Kutta step of the function f with respect to y
        Parameters
        ----------
        f : a function of x and y 
            The function that is being considered and to be used in RK4
        x : float 
            Initial value of x
        y: array
            An array of y values
        h: float
            Value of the step size used
        Returns
        -------
        An array containing values of f after taking one Runge-Kutta step for each value of (x,y)."""
        k1 = h*f(x, y)
        k2 = h*f(x + 0.5*h, y + 0.5*k1)
        # complete the following lines
        k3 = h*f(x + 0.5*h, y + 0.5*k2)
        k4 = h*f(x + h, y + k3 )
        return np.array(y + k1/6. + k2/3. + k3/3. + k4/6.)             

    def update(self, frame, line, f, x, h, pstep):
        # add a docstring to this function
        """ Updates the set of (x,y) values
        ----------
        f : a function of x and y 
            The function that is being considered and to be used in RK4
        x : float 
            x value
        y: array
            An array of y values
        h: float
            Value of the step size used
        Returns
        -------
        Lists containing the values of x and y
        """
        if self.poncaire:
            self.y.append(self.rk4_step(f, frame*h, self.y[-1], pstep))
        else:
            self.y.append(self.rk4_step(f, frame*h, self.y[-1], h))
        xs, ys = zip(*self.y)
        line.set_xdata(xs)
        line.set_ydata(ys)
    
    def continue_loop(self):
        # add a docstring to this function
        """ Updates and generates the index of iteration
        ----------
        Returns
        -------
        Outputs a generator object
        """
        i = 0
        while 1:
            i += 1
            # what does yield do?
            ###ANSWER: It also functions like return, except that yield returns a generator
            # what will happen if return is used instead of yield?
            ###ANSWER: An error will be encountered (TypeError: int object is not an iterator)
            yield i
    
    def on_key(self, event):
        key = event.key
        if key == 'i':
            self.a += 0.01
            self.info_text.set_text("$r$ = %0.2f\t$a$ = %0.6f" % (self.r,
                                                                self.a))
            # add analogous code such that pressing "d" will decrease a by 0.01
        elif key == 'd':
            self.a -= 0.01
            self.info_text.set_text("$r$ = %0.2f\t$a$ = %0.59f" % (self.r,
                                                                  self.a))
        
        elif key == '+':
            self.lims /= 2
            plt.xlim(-self.lims, self.lims)
            plt.ylim(-self.lims, self.lims)
        # add analogous code such that pressing "-" will zoom out the plot
        elif key == '-':
            self.lims *= 2
            plt.xlim(-self.lims, self.lims)
            plt.ylim(-self.lims, self.lims)
                                                                  
        elif key == 't':
            self.poncaire = not self.poncaire
            if self.poncaire:
                self.line.set_linestyle('')
                self.line.set_marker('.')
                self.y = [np.array([np.pi/2, 0])]
            else:
                self.line.set_linestyle('-')
                self.line.set_marker('')
                self.y = [np.array([np.pi/2, 0])]
    
    def run(self):
        fig = plt.figure()
        # what are the other possible events?
        fig.canvas.mpl_connect('key_press_event', self.on_key)
    
        self.y = [np.array([np.pi/2, 0])]
        self.line, = plt.plot([], [])
        plt.xlim(-self.lims, self.lims)
        plt.ylim(-self.lims, self.lims)
        self.info_text = plt.figtext(0.15, 0.85, "$r$ = %0.2f\t$a$ = %0.6f" % (self.r, self.a))
        anim = FuncAnimation(fig, self.update, self.continue_loop, 
                             fargs=(self.line, self.derivs, 0, self.dt, self.pstep),
                             interval=25, repeat=False)
        plt.show()

if __name__ == "__main__":
    sim = Simulation()
    sim.run()

