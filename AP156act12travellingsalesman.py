# -*- coding: utf-8 -*-
"""
Created on Sat Dec 03 00:30:43 2016

@author: Nica Ferrer
"""


# -*- coding: utf-8 -*-
"""
Created on Fri Dec 02 20:43:35 2016

@author: Nica Ferrer
"""

import numpy as np
import matplotlib.pyplot as plt
import time

L = 10
N = 100

square = np.zeros([L,L])
#N cities are randomly distributed in a square of size L
Cx = []
Cy = []
for i in xrange(0,N):
    x = np.random.randint(11)
    y = np.random.randint(11)
    Cx.append(x)
    Cy.append(y)
#####################################################
Svals = []
S = [i for i in xrange(0,N)]
#Sx = []
#Sy = []
#for i in S:
#    Sx.append(Cx[i])
#    Sy.append(Cy[i])
#Sx.append(Cx[0])
#Sy.append(Cy[0])
#plt.plot(Sx,Sy)
#plt.scatter(Cx,Cy)
#plt.title('1st')

Svals.append(S)
#####################

def D(S,Cx,Cy):
    distance0 = 0
    for i in xrange(0,N-2):
        x1 = Cx[S[i]]
        y1 = Cy[S[i]]
        x2 = Cx[S[i+1]]
        y2 = Cy[S[i+1]]
        dx = x2-x1
        dy = y2-y1
        dist = np.sqrt(dx**2+dy**2)
        distance0 +=dist
    return distance0
#####################
def newS(S,p,l):
    Sprime = []
    if p==0 and l==0:
        Sprime = S
    elif p==0 and l!=0:
        for i in xrange(0,l+1):
            j = (l)-i
            Sprime.append(S[j])
        for i in xrange(l+1,N):
            Sprime.append(S[i])
    elif p!=0 and l==0:
        Sprime = S
    elif p!=0 and l!=0:
        for i in xrange(0,p-1):
            Sprime.append(S[i])
        for i in xrange(0,l+1):
            j = (p+l-1)-i
            Sprime.append(S[j])
        for i in xrange(p+l,N):
            Sprime.append(S[i])
    return Sprime
#####################
distances= []

T = 100./8 # L/8
ti = time.clock()
di = D(S,Cx,Cy)
distances.append(di)
while T>1e-20:


    for i in xrange(0,100):
        d0 = D(S,Cx,Cy)
        
        p = np.random.randint((N/2) - 1)
        l = np.random.randint((N/2) - 1)


        Sprime = newS(S,p,l)
        #Svals.append(Sprime)
        dp = D(Sprime,Cx,Cy)
        #distances.append(dp)
        
        if dp<=d0:
            prob = 1
            #accept
            S = Sprime
    #         Sx = []
    #         Sy = []
    #         for i in S:
    #             Sx.append(Cx[i])
    #             Sy.append(Cy[i])
    #         Sx.append(Cx[S[0]])
    #         Sy.append(Cy[S[0]])
    #         plt.plot(Sx,Sy)
    #         plt.scatter(Cx,Cy)
    #         plt.show()
        elif dp>d0:
            bf = np.exp(-(dp-d0)/T)
            r = np.random.random()
            #print 'bf:', bf
            #print 'r', r
            if r<bf:
                #print "accepted"
                #accept
                S = Sprime
        #             Sx = []
        #             Sy = []
        #             for i in S:
        #                 Sx.append(Cx[i])
        #                 Sy.append(Cy[i])
        #             Sx.append(Cx[S[0]])
        #             Sy.append(Cy[S[0]])
        #             plt.plot(Sx,Sy)
        #             plt.scatter(Cx,Cy)
        #             plt.show()
        #             stop+=2
        #             print "yey"
    T = T- (0.1/100)*T
    print T
#plt.show()    
print "initial distance: ", di
print "final distance:", distances[len(distances)-1]
(j,) = np.unravel_index(np.argmin(distances),np.shape(distances))  
tf = time.clock()
print "shortest distance:", distances[j]
print "processing time:", tf-ti
#Sx = []
#Sy = []
#for i in S:
#    Sx.append(Cx[i])
#    Sy.append(Cy[i])
#Sx.append(Cx[S[0]])
#Sy.append(Cy[S[0]])
#plt.plot(Sx,Sy)
#plt.scatter(Cx,Cy)
#plt.show()

#Sbest = Svals[j]
Sx = []
Sy = []
for i in S:
    Sx.append(Cx[i])
    Sy.append(Cy[i])
Sx.append(Cx[S[0]])
Sy.append(Cy[S[0]])
plt.plot(Sx,Sy)
plt.scatter(Cx,Cy)
plt.show()