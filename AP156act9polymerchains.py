
# coding: utf-8

# #### AP 156
# #### Nica Jane Ferrer
# #### 2013-25765
# ##### Polymer Chains
# - Simulate polymer chains using a self-avoiding random
# walks.
# 

# In[1]:

import numpy as np
import matplotlib.pyplot as plt
L = 15
N = 7
def genspace():
    space = np.zeros([L,L])
    for i in xrange(0,L):
        #top row
        space[i,0] = 1
        #bottom row
        space[i,L-1] = 1
        #leftmost column
        space[0,i] = 1
        #rightmost column
        space[L-1,i] = 1
    return space






#1. start with a configuration on the lattice, 
#labelling the ends of the chain as head and tail respectively
conf0x = np.array([[4, 4, 4, 3, 2, 2, 2, 2]])
conf0y = np.array([[2, 3, 4, 4, 4, 5, 6, 7]])

space = genspace()
j = 1
for i in xrange(0,N):
    x = conf0x[0][i]
    y = conf0y[0][i]
    space[x,y] = j
    j+=1

plt.matshow(space)
plt.show()
oldcx = conf0x
oldcy = conf0y

#######################

#2. remove the last monomer at the tail end and randomly select one of the sites
#adjacent to the head (three possibilities on the square lattice)
def removemono(val,oldcx,oldcy):
    nconfx = np.zeros([1,N])
    nconfy = np.zeros([1,N])
    if val==1:
        #head!
        for i in xrange(0,N-1):
            nconfx[0][i+1] = oldcx[0][i]
            nconfy[0][i+1] = oldcy[0][i]
    elif val==-1:
        #tail
        for i in xrange(0,N-1):
            j = N-1
            nconfx[0][j-i-1] = oldcx[0][j-i]
            nconfy[0][j-i-1] = oldcy[0][j-i]
    return nconfx, nconfy


#3. if the site is free, add a new head monomer there. if it is
#occupied, restore the old configuration, interchange the labels head and tail,
#and take the otherwise unmodified configuration into account in calculating averages

def move(x,y,xn,yn):
    dx = x - xn
    dy = y - yn

    n = np.random.randint(3)
    #xhat
    if dx==1 and dy==0:
        #right
        if n==0:
            x+=1
        #up
        elif n==1:
            y-=1
        #down
        elif n==2:
            y+=1
    #yhat
    elif dx==0 and dy== 1:
        #up
        if n==0:
            y-=1
        #right
        elif n==1:
            x+=1
        #left
        elif n==2:
            x-=1
    #-xhat
    elif dx==-1 and dy==0:
        #left
        if n==0:
            x-=1
        #up
        elif n==1:
            y-=1
        #down
        elif n==2:
            y+=1
    #-yhat
    elif dx==0 and dy== -1:
        #down
        if n==0:
            y+=1
        #left
        elif n==1:
            x-=1
        #right
        elif n==2:
            x+=1
    return x,y

def headortail(num,oldcx,oldcy):
    if num==1:
        #head
        x = oldcx[0][0]
        xn = oldcx[0][1]
        y = oldcy[0][0]
        yn = oldcy[0][1]
    elif num==-1:
        #tail
        x = oldcx[0][N-1]
        xn = oldcx[0][N-2]
        y = oldcy[0][N-1]
        yn = oldcy[0][N-2]
    return x,xn,y,yn


fig, ax = plt.subplots()

#4. Iterate steps 2 and 3
#MAIN
val = 1
for i in xrange(0,20):
    x,xn,y,yn = headortail(val,oldcx, oldcy)
    newconfx,newconfy = removemono(val,oldcx,oldcy)
    nx,ny = move(x,y,xn,yn)
    if space[int(nx), int(ny)]!=0:
        val = -val
    elif space[int(nx), int(ny)]==0:
        if val==1:
            newconfx[0][0] = nx
            newconfy[0][0] = ny
        elif val==-1:
            newconfx[0][N-1] = nx
            newconfy[0][N-1] = ny
        space = genspace()
        j = 1
        for i in xrange(0,N-1):
            x = newconfx[0][i]
            y = newconfy[0][i]
            space[x,y] = j
            j+=1
        oldcx = newconfx
        oldcy = newconfy
        plt.matshow(space)
        plt.show()


# In[ ]:




# In[ ]:



