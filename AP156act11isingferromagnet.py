
# coding: utf-8

# #### AP 156
# #### Nica Jane Ferrer
# #### 2013-25765
# ##### Ising ferromagnet
# - Construct an interactive program for an Ising
# ferromagnet.

# In[33]:

#############################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

class Simulation(object):
    
    #constants
#     J = 1.0
#     h = 0.0
#     kb = 1
#     T = 1
    def initvals(self,J  ,T , kb, h):
        self.J = J
        self.T = T
        self.kb = kb
        self.h = h


    
    def E(self,J,h):
        N = 20
        prodbycolumn = np.zeros([N-1,N-1],float)
        prodbyrow = np.zeros([N-1,N-1],float)
        for i in xrange(0,N-1):
            prodbyrow[i,0:N-1] = self.modspins[i,0:N-1]*self.modspins[i+1,0:N-1]
            prodbycolumn[0:N-1,i] = self.modspins[0:N-1,i]*self.modspins[0:N-1,i+1]

#         J = 1.0
        E = 0.0

        for x in xrange(0,N-1):
            for y in xrange(0,N-1):
                E+=-self.J*prodbyrow[x,y]
                E+=-self.J*prodbycolumn[x,y]
            
        sp = 0.
        for i in xrange(0,N):
            for j in xrange(0,N):
                spin=self.modspins[i,j]
                sp+=1
        E = E + self.h*sp
        return E
    
    
    
    def update(self, frame, im):
        #constants
        
        E1 = self.E(self.J,self.h)
        ###Flipping of the randomly chosen spins
        c = np.random.randint(0,20)
        d = np.random.randint(0,20)
        if self.modspins[c,d]==-1:
            self.modspins[c,d] = 1
        elif self.modspins[c,d] ==1:
            self.modspins[c,d]= -1
        ###

        E2 = self.E(self.J,self.h)
        deltaE = E1 - E2

        if deltaE>=0:
            probability = 1
            E1 = E2
        elif deltaE<0:
            prob = np.exp(deltaE/(self.kb*self.T))
            r = np.random.random()
            if r<prob:
                #proceed with modspins
                E1 = E2
            elif r>prob:
                #rejected! undo flipping of spin
                if self.modspins[c,d] ==1:
                    self.modspins[c,d]=-1
                elif self.modspins[c,d] ==-1:
                    self.modspins[c,d] = 1
#                 calculate the energy after the spins are flipped back due to rejection
#                 E1 = self.E(self.modspins)
        im.set_array(self.modspins)
    
    def continue_loop(self):
        i = 0        
        while 1:
            i+=1
            yield i
    
    def on_key(self, event):
        #J,h,T
        key = event.key
        if key == 'j':
            self.J += 0.1
            self.info_text.set_text("$T$ = %0.2f\t$J$ = %0.6f\t$h$ = %1.0f" % (self.T, self.J, self.h))
     
        elif key == 'l':
            self.J -= 0.01
            self.info_text.set_text("$T$ = %0.2f\t$J$ = %0.6f\t$h$ = %1.0f" % (self.T, self.J, self.h))
        elif key == 'u':
            self.h += 0.1
            self.info_text.set_text("$T$ = %0.2f\t$J$ = %0.6f\t$h$ = %1.0f" % (self.T, self.J, self.h))
        
        elif key == 'd':
            self.h -= 0.01
            self.info_text.set_text("$T$ = %0.2f\t$J$ = %0.6f\t$h$ = %1.0f" % (self.T, self.J, self.h))
            
        elif key == 't':
            self.T += 0.1
            self.info_text.set_text("$T$ = %0.2f\t$J$ = %0.6f\t$h$ = %1.0f" % (self.T, self.J, self.h))
           
        elif key == 'b':
            self.T -= 0.01
            self.info_text.set_text("$T$ = %0.2f\t$J$ = %0.6f\t$h$ = %1.0f" % (self.T, self.J, self.h))
        
        

    def run(self):

        self.initvals(J = 1.0 ,T = 1.0, kb=1, h=0)
        N = 20
        self.modspins = np.zeros([N,N],float)
        for n in xrange(0,N):
            for m in xrange(0,N):
                spin = (np.random.randint(0,2))-1
                if spin==0:
                    spin +=1
                self.modspins[n,m] = spin
        fig = plt.figure()
        plt.ylim(0,25)
        fig.canvas.mpl_connect('key_press_event', self.on_key)
        self.im = plt.imshow(self.modspins,interpolation='nearest',cmap='gray')
#         self.info_text = plt.figtext(0.15, 0.85, "$T$ = %0.2f: press t to + or b to -"  % (self.T) )
#         self.info_text = plt.figtext(0.15, 0.85, "$J$ = %0.6f press j to + or l to -"  % (self.J) )
#         self.info_text = plt.figtext(0.15, 0.85, "$h$ = %1.0f press u to + or d to -"  % (self.h) )
        self.info_text = plt.figtext(0.15, 0.85, "$T$ = %0.2f: press t to + or b to - , $J$ = %0.6f press j to + or l to - , $h$ = %1.0f press u to + or d to -" % (self.T, self.J, self.h))
        anim = FuncAnimation(fig, self.update, self.continue_loop, 
                             fargs=[self.im], interval=25, repeat=False)
        plt.show()
        
sim = Simulation()
sim.run()


# In[ ]:




# In[ ]:



