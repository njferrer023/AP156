
# coding: utf-8

# #### AP 156
# #### Nica Jane Ferrer
# #### 2013-25765
# ##### Fractal Aggregates
# - Construct various patterns using diffusion-limited
# aggregation.

# In[42]:

import numpy as np
import matplotlib.pyplot as plt
import time

#constants
rmax = 1.
lmax = 220.
rs = rmax + 3.
rd = rmax + 5.
rkill = 100.*rmax
xf = np.zeros([lmax,lmax])
N = 3000.
rx = 0.
ry = 0.
#which coordinates have particles
RX = []
RY = []

def occupy(rs,rmax):
    phi = np.random.random()*2.*np.pi
    rx = rs*np.sin(phi)
    ry = rs*np.cos(phi)
    return int(rx), int(ry)

def jump(rx,ry):
    r = np.random.randint(4)
    if r==0:
        rx+=1
    elif r==1:
        rx+= -1
    elif r==2:
        ry+=1
    elif r==3:
        ry+=-1
    return int(rx),int(ry)
#after each jump:check whether
#1. particle is annihilated (R>R_k)
#2. it has reached a site adjacent to the aggregate
#3. a short jump (R<R_d) or a long 'circle jump' (R>=R_d)
# needs to be performed in the next step

def check(rx,ry):    
    r=np.sqrt(rx**2+ry**2)
    #k
    if r>rkill:
        val= 'k'
    #c
    elif r>=rd:
        val = 'c'
    #a
    elif (xf[rx + 1 + lmax/2][ry + lmax/2] + xf[rx-1 + lmax/2][ry + lmax/2]+           xf[rx + lmax/2][ry + 1 + lmax/2] + xf[rx + lmax/2][ry - 1 +lmax/2]>0):
        val = 'a'
    #j
    else:
        val = 'j'
    return val

def aggregate(rmax,rx,ry):
    i =rx+ lmax/2
    j =ry+ lmax/2
    xf[i][j] = 1
    x = rx
    y = ry
    rmax = max(rmax, np.sqrt(x**2+y**2))
    return rmax
# #     circle(4*rx+340,4*ry+240,2)
#     if rmax > (lmax/2. -3):
#         stop = 1
#     elif rmax < (lmax/2. -3):
#         stop = 0
#     RX.append(rx)
#     RY.append(ry)
#     stopv = stop
#     return rx,ry,stopv,rmax

def circlejump(rx,ry,rs):
    phi = np.random.random()*2.*np.pi
    r = np.sqrt(rx**2+ry**2)
    rx+= (r-rs)*np.sin(phi)
    ry+= (r-rs)*np.cos(phi)
    return int(rx),int(ry)


rx= ry= lmax/2
RX.append(rx)
RY.append(ry)
xf[rx,ry]=1




for i in xrange(0,3000):
    rx,ry = occupy(rs,rmax)
    rx,ry = jump(rx,ry)
    while 1:
        val = check(rx,ry)
        if val == 'k':
            nrx, nry = occupy(rx,ry)
            rx,ry = jump(nrx,nry)
        elif val == 'a':
            rmax = aggregate(rmax,rx,ry)
            rs = rmax + 3.
            rd = rmax + 5.
            rkill = 100.*rmax
            RX.append(rx)
            RY.append(ry)
            break
        elif val == 'j':
            rx,ry = jump(rx,ry)
        elif val == 'c':
            rx,ry = circlejump(rx,ry,rs)
plt.matshow(xf)
plt.show()

