
# coding: utf-8

# In[1]:

####AP156
##Ferrer, Nica Jane
####2013-25765

#Sierpinski Gasket
import numpy as np
import matplotlib.pylab as plt
qisx = []
qisy = []
px = [1,3,2]
py = [0,2,5]
q0x = -1
q0y = 2

for i in xrange(0,10000):
    num = np.random.randint(0,3)
    nq0x = (px[num]+q0x)/2.0 
    nq0y = (py[num]+q0y)/2.0
    qisx.append(nq0x)
    qisy.append(nq0y)
    q0x = nq0x
    q0y = nq0y
plt.scatter(px,py)
plt.scatter(q0x,q0y)
plt.scatter(qisx,qisy)
plt.show()


# In[ ]:



