# -*- coding: utf-8 -*-
"""
Created on Fri Dec 02 22:50:32 2016

@author: Nica Ferrer
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Dec 02 22:16:43 2016

@author: Nica Ferrer
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 21:29:36 2016

@author: Nica Ferrer
"""



###AS OF Nov 22
import numpy as np
import matplotlib.pyplot as plt
import time


def grid(p,L):
    return np.random.random([L,L])<p
def identf(grid, val):
    return np.sum(grid==val)


def labeledgridmap(grid,L):
    labeledgrid = np.zeros([L,L])
    mapping= []
    
    mapfrom = []
    for i in xrange(0,L**2):
        mapfrom.append([])
        

    
    m = 1
    for j in xrange(0,L):
        for i in xrange(0,L):
            
            if grid[i,j]==True:
                #grid[0,0]
                if j==0 and i==0:
                    labeledgrid[i,j]=m
                    m+=1
                    
                #for other grid[i,j]'s  
                #first column
                elif j==0 and i>0:
                    uplabel = labeledgrid[i-1,j]
                    if uplabel!=0:
                        labeledgrid[i,j] = uplabel
                    elif uplabel==0:
                        labeledgrid[i,j] = m
                        m+=1
                #first row        
                elif i==0 and j>0:
                    leftlabel = labeledgrid[i,j-1]
                    if leftlabel!=0:
                        labeledgrid[i,j] = leftlabel
                    elif leftlabel==0:
                        labeledgrid[i,j] = m
                        m+=1
                #general case      
                elif i>0 and j>0:
                    leftlabel = labeledgrid[i,j-1]
                    uplabel = labeledgrid[i-1,j]
                    if leftlabel==0 and uplabel==0:
                        labeledgrid[i,j] = m
                        m+=1
                    elif leftlabel==0 and uplabel!=0:
                        labeledgrid[i,j]=uplabel
                    elif leftlabel!=0 and uplabel==0:
                        labeledgrid[i,j]=leftlabel
                    elif uplabel!=0 and leftlabel!=0:
                        
                        if uplabel-leftlabel>0:

                            labeledgrid[i,j] = leftlabel
                            mapping.append((leftlabel,uplabel))
                            
                            to = int(leftlabel)
                            mapfrom[to].append(uplabel)
                        elif uplabel-leftlabel<0:

                            labeledgrid[i,j] = uplabel
                            mapping.append((uplabel,leftlabel))
                            to = int(uplabel)
                            mapfrom[to].append(leftlabel)
                            
                            

            elif grid[i,j]==False:
                labeledgrid[i,j]=0

    x = len(mapping)
    yvals = []
    xvals = []
    for i in xrange(0,x):
        xvals.append(mapping[i][0])
        yvals.append(mapping[i][1])
            

    return labeledgrid, mapping, x, mapfrom, xvals, yvals
    



print '-------------------------------'


def relabel(labeledgrid,X,Y,l,mapfrom):
    #sort!
    T = len(mapfrom)
    for i in xrange(0,T):
        mapfrom[i].sort()
    
    for m in xrange(0,T):
        j = T-m-1
        vals = list(set(mapfrom[j]))
        if len(vals)==0:
            pass
        elif len(vals)!=0:
            for k in xrange(0,len(vals)):
                l = len(vals)-k-1
                i = vals[l]
                labeledgrid = labeledgrid-labeledgrid*(labeledgrid==i)+ j*(labeledgrid==i)

    return labeledgrid
print '--------------------------------------'


def finalrelabel(updatedgrid,L):
    listvals = []
    for j in xrange(0,L):
        for i in xrange(0,L):
            n = updatedgrid[i,j]
            if n==0:
                pass
            elif n!=0:
                listvals.append(n)
    k = list(set(listvals))
    m = len(k)+1
    finalvalues = [x for x in xrange(1,m)]
    numclus = len(finalvalues)

    for b in xrange(0, m-1):
        i = k[b]
        updatedgrid = updatedgrid-updatedgrid*(updatedgrid==i)+ finalvalues[b]*(updatedgrid==i)

    
    
    return updatedgrid, numclus


print '--------------------------------------'

def checkclus(finalgrid, numclus, L):
    N = L-1
    frow = np.zeros([1,numclus])
    lrow = np.zeros([1,numclus])
    fcol = np.zeros([1,numclus])
    lcol = np.zeros([1,numclus])

    for i in xrange(0,L):
        #first row
        frowel = finalgrid[0,i]
        if frowel==0:
            pass
        elif frowel!=0:
            j = frowel
            j-=1
            frow[0,j] = 1
        #last row
        lrowel = finalgrid[N,i]
        if lrowel==0:
            pass
        elif lrowel!=0:
            j = lrowel
            j -=1
            lrow[0,j] = 1
        #first column   
        fcolel = finalgrid[i,0]
        if fcolel==0:
            pass
        elif fcolel!=0:
            j = fcolel
            j-=1
            fcol[0,j] = 1
        #last column   
        lcolel = finalgrid[i,N]
        if lcolel==0:
            pass
        elif lcolel!=0:
            j = lcolel
            j-=1
            lcol[0,j] = 1

    removeclus = []
    for k in xrange(0, numclus):
        a = frow[0,k]
        b = lrow[0,k]
        c = fcol[0,k]
        d = lcol[0,k]
        if (a==1 and b==1) or (c==1 and d==1):
            #infinite cluster!
            removeclus.append(k+1)      
    return removeclus

print '--------------------------------------'

def finiteclus(removeclus,numclus, L):

    clusters = [x for x in xrange(1,numclus+1)]
    M = len(removeclus)
    if M==0:
        pass
    elif M!=0:
        for i in removeclus:
            clusters.remove(i)
    return clusters
print '--------------------------------------'

def smean(finiteclus,finalgrid):
    numcomps = []
    for i in finiteclus:
        num = identf(finalgrid,i)
        numcomps.append(num)
    smeanv = (1.0*sum(numcomps))/(1.0*len(numcomps))
    return smeanv


ti = time.clock()
L = 100
pvals = np.linspace(0.1,0.9,100)
svals = []
for p in pvals:
    smvals = []
    removecl = []
    for i in xrange(0,30):
        labeledgrid, mapping, l, mapfrom, X, Y = labeledgridmap(grid(p,L), L)
        
        relabeledgrid = relabel(labeledgrid,X,Y,l,mapfrom)
        
        finalgrid, numclus = finalrelabel(relabeledgrid,L)
        
        removeclusters = checkclus(finalgrid, numclus,L)
        removecl.append(len(removeclusters))
        
        labels = np.unique(finalgrid)
        numcomps = []
        possibleinf = []
        for i in xrange(1,len(labels)):
            components = identf(finalgrid, labels[i])
            numcomps.append(components)
            if components>L:
                possibleinf.append(labels[i])
        
        finiteclusters = finiteclus(removeclusters, numclus, L)
        smvalue = smean(finiteclusters,finalgrid)
        
        smvals.append(smvalue)
       
    avef = (1.0*sum(smvals))/(1.0*len(smvals))
    
    svals.append(avef)
   
    print 'p = ', p
tf = time.clock()
print 'processing time:', tf-ti
i = np.unravel_index(np.argmax(svals), np.shape(svals))
print 'p_c = ', pvals[i]
plt.plot(pvals,svals)
plt.scatter(pvals,svals)
plt.show()